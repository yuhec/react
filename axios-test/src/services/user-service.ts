import {httpService} from './index'
import RandomUser from '../entity/RandomUser'

type Params = {
    results?: number,
    page?: number,
    nat?: string | null
}
class UserService {
    getUsers({results=5, page=1, nat=null}: Params) : Promise<RandomUser> {
        return httpService.get('', {
            params: {
                results,
                page,
                nat,
            }
        })
    }
}

const userService = new UserService()

export default userService