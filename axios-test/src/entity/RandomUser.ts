import User from './User'

type RandomUser = {
    data: {
        results: User[]
    }
}
export default RandomUser