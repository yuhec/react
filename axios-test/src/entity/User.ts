type User = {
    name: {
        first: string,
        last: string,
      },
      picture: {
        large: string,
      }
      nat: string
}
export default User