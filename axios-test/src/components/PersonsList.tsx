import { useEffect, useState } from 'react';

import { TextField, GridListTile, Grid, GridList, Box } from '@material-ui/core';
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';

import PersonDetail from './PersonDetail'

import {userService} from '../services/index'
import User from '../entity/User'
import RandomUser from '../entity/RandomUser'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
  }),
);

// TODO DISPLAY LOADING AND MANAGE ERROR
// AUTOCOMPLETE FOR NAT SEARCH
function PersonsList() {
  const [users, setUsers] = useState<User[]>([])
  const [error, setError] = useState(null)
  const [loading, setLoading] = useState<boolean>(false)
  const [search, setSearch] = useState<string | null>()

  const [page, setPage] = useState(1)
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  function loadUsers() {
    userService.getUsers({page, nat: search})
      .then((res: RandomUser) => {
        setLoading(true)
        setUsers(res.data.results)
        console.log(users)
      })
      .catch((err) => console.log(err))
      .finally(() => { setLoading(false) })
  }

  useEffect(() => {
    loadUsers()
  }, [search, page])


  const classes = useStyles();

  return (
    <div className="App">
      <Grid container justify="center">
        <Grid item md={7} className="center pa-20">
          <TextField
            value={search}
            label="Recherche par nationalité"
            onChange={(e) => {
              setSearch(prevState => {
                let val = prevState
                val = e.target.value
                return val
              })
            }}
          />
        </Grid>
        <Grid item md={10}>
          <div className={classes.root}>

            <GridList>
              {
                users.map((user, i) => {
                  return (
                    <GridListTile key={i} className="person-card">
                      <PersonDetail
                        user={user}
                      />
                    </GridListTile>
                  )
                })
              }
            </GridList>
          </div>
        </Grid>

        <Grid item md={5} className="center pa-20">
          <Box
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <Pagination count={10} color="primary" page={page} onChange={handleChange} />
          </Box>
        </Grid>

      </Grid>

    </div>
  );
}

export default PersonsList;
