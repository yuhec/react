import { useEffect, useState } from 'react';

import { Card, Avatar, Grid, GridListTileBar } from '@material-ui/core';

type Props = {
  user: {
    name: {
      first: string,
      last: string,
    },
    picture: {
      large: string,
    }
    nat: string
  }
}
function PersonDetail(props: Props) {
  return (
    <div>
      <img src={props.user.picture.large} />
      <GridListTileBar
        title={`${props.user.name.first} ${props.user.name.last}`}
        subtitle={<span>Nat: {props.user.nat}</span>}
      />
    </div>
  );
}

export default PersonDetail;
