import './App.css';
import PersonList from './components/PersonsList'

function App() {
  return (
    <div className="App">
      <PersonList />
    </div>
  );
}

export default App;
