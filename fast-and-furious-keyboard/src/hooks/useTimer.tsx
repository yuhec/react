import { useEffect, useState } from "react";

function useTimer(initialTime: number) {
    const [progress, setProgress] = useState(initialTime)

    useEffect(() => {
        const timer = setInterval(() => {
          setProgress((oldProgress) => {
            if (oldProgress <= 0) {
              return 0;
            }
            return oldProgress - (100/10);
          });
        }, 500);

        return () => {
            clearInterval(timer);
        };
    }, [initialTime]);
    return progress;
}
export default useTimer