import React from 'react';

import { Container, Grid, Button } from '@material-ui/core';

type Props = {
    nbWords: number,
    time: number,
    retry: () => void
}
function Stats(props: Props) {
    const wordsPerMinute = (props.nbWords * 60) / props.time
    return (
        <div>
            <Container>
                <header>Stats</header>
                <Grid container justify="center">
                    <Grid item md={4}>
                        <p>Words completed : {props.nbWords}</p>
                        <p>Time : {props.time}</p>
                        <p>Words per minute : {wordsPerMinute}</p>
                        <Button variant="outlined" color="primary" onClick={props.retry}>Retry</Button>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}

export default Stats;
