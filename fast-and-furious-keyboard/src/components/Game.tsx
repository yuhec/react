import { useState, useEffect } from 'react';

import RandomWords from 'random-words'

import Board from './Board'
import Stats from './Stats'

function Game() {
    const initTime = 10
    const [seconds, setSeconds] = useState(initTime)
    const [randomWord, setRandomWord] = useState(RandomWords())
    const [nbWords, setNbWords] = useState(0)
    const [word, setWord] = useState('')

    const [launched, setLaunched] = useState(false)
    const [progress, setProgress] = useState(100)

    // TODO : THERE IS A DELAY WHEN FIRST LETTER AND CHRONO
    useEffect(() => {
        const timer = setInterval(() => {
            if (launched) {
                setProgress((oldProgress) => {
                    if (oldProgress <= 0) {
                        setLaunched(false)
                        return 0;
                    }
                    return oldProgress - (100 / 10);
                });
                setSeconds((old) => {
                    return old <= 0 ? 0 : old -1
                })
            }
        }, 500);

        return () => {
            clearInterval(timer);
        };
    }, [launched]);

    function initTimer() {
        setRandomWord(RandomWords())
        setWord('')
        setNbWords(0)
        setProgress(100)
        setSeconds(initTime)
    }

    function checkWord(val: string) {
        if (!launched) {
            setLaunched(true)
        }
        if (val === randomWord) {
            setRandomWord(RandomWords())
            setWord('')
            setNbWords((prevValue) => prevValue + 1)
        } else {
            const randomWordInProgress = randomWord.slice(0, val.length)
            if (val === randomWordInProgress) {
                setWord(val)
            }
        }
    }

    return (
        <div className="game">
            {
                progress > 0 ?
                    <Board
                        seconds={seconds}
                        progress={progress}
                        checkWord={(val: string) => checkWord(val)}
                        nbWords={nbWords}
                        randomWord={randomWord}
                        word={word}
                    /> :
                    <Stats
                        time={initTime}
                        nbWords={nbWords}
                        retry={() => initTimer()}
                    />
            }

        </div >
    );
}

export default Game;
