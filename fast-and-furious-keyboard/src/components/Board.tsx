import { LinearProgress, Grid } from '@material-ui/core';

type Props = {
    seconds: number
    progress: number,
    nbWords: number,
    checkWord: (val: string) => void,
    randomWord: string,
    word: string,
}

function Board(props: Props) {
    return (
        <div className="game">
            <Grid container justify="center">
                <Grid item md={4}>
                    <Grid container justify="space-between">
                        <Grid item md={4}>
                            {props.seconds}s
                    </Grid>
                        <Grid item md={4} className="text-align-right">
                            {props.nbWords}
                        </Grid>
                    </Grid>
                    <LinearProgress variant="determinate" value={props.progress} />
                </Grid>
            </Grid>
            <Grid container justify="center" style={{marginTop: '20px'}}>
            <Grid item md={4} className="word">
                <div className="relative">
                    <input
                        className="input-word"
                        value={props.word}
                        onChange={(e) => props.checkWord(e.target.value)}
                    >
                    </input>
                    <div className="current-word">{props.randomWord}</div>
                </div>
            </Grid>
        </Grid>
    </div >
  );
}

export default Board;
