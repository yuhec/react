import React from 'react';
import logo from './logo.svg';
import './App.css';
import Game from './components/Game'

import {Container} from '@material-ui/core';

function App() {
  return (
    <div>
      <Container>
        <header>Fast and Furious Keyboard</header>
        <Game />
      </Container>
    </div>
  );
}

export default App;
