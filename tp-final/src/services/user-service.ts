import User from "../entities/User"

const users : User[] = [
  {
    name: 'regular',
    email: 'r@o.fr',
    password: 'regular',
    regular: true
  },
  {
    name: 'admin',
    email: 'a@o.fr',
    password: 'admin',
    admin: true
  },
  {
    name: 'super_admin',
    email: 'sa@o.fr',
    password: 'super_admin',
    super_admin: true
  }
]

class UserService {
    _user:User = {
      name: '',
      password: '',
      email: '',
    }
    isLogged = false
    login(email: string, password: string): Promise<User> {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          const user = users.find((user) => user.email === email && user.password === password)
          if (!!user) {
            this._user = user
            this.isLogged = true
            resolve(this._user)
          } else {
            reject('Incorrect, try again...')
          }
        }, 1000)
      })
    }
    logout() {
      return new Promise((resolve) => {
        this.isLogged = false
        setTimeout(() => {
          this.isLogged = false
          this._user = {
            name: '',
            password: '',
            email: ''
          }
          resolve(this._user)
        }, 1000)
      })
    }
    create(item: User): Promise<User> {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          const alreadyExist = users.find((user) => user.name === item.name && user.email === item.email)
          if (!!alreadyExist) {
            reject('This email and name already exist')
          } else {
            item.regular = true
            users.push(item)
            this._user = item
            this.isLogged = true
            resolve(this._user)
          }
        }, 1000)
      })
    }
    update(item: User): Promise<User> {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          const userIndex = users.findIndex((user) => user.name === item.name)
          if (userIndex > 0) {
            users[userIndex].email = item.email
            users[userIndex].password = item.password
            this._user = users[userIndex]
            resolve(this._user)
          } else {
            reject('User not found')
          }
        }, 1000)
      })
    }
    delete(item: User): Promise<void> {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          const userIndex = users.findIndex((user) => user.name === item.name)
          users.slice(userIndex, 1)
          resolve()
        }, 1000)
      })
    }
}
const userService = new UserService()
export default userService