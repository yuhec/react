import {v4 as uuidv4} from 'uuid'
import { LoremIpsum } from 'lorem-ipsum'
import Article from '../entities/Article';
import { rejects } from 'assert';

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 12,
    min: 7
  }
});

function generateArticle() {
  return {
    id: uuidv4(),
    title: lorem.generateSentences(1),
    text: lorem.generateParagraphs(3)
  }
}

class ArticlesService {
    _articles:Article[] = [
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
    ]
    get(page: number): Promise<Article[] | []>{
      return new Promise((resolve) => {
        setTimeout(() => {
          const articles = this._articles.slice(page-1, 5)
          resolve(articles)
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
    getById(id: string): Promise<Article>{
      return new Promise((resolve, rejects) => {
        setTimeout(() => {
          const article = this._articles.find(article => article.id === id)
          if (!!article) {
            resolve(article)
          } else {
            rejects('Article not found')
          }
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
    update(id: string, item: Article): Promise<void>{
      return new Promise((resolve, rejects) => {
        setTimeout(() => {
          const index = this._articles.findIndex(article => article.id === id)
          if (index > 0) {
            this._articles[index].title = item.title
            this._articles[index].text = item.text
            resolve()
          } else {
            rejects('Article not found')
          }
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
}
const articlesService = new ArticlesService()
export default articlesService