import { Container, Card } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import '../App.css'
import { articlesService } from "../services";
import Article from "../entities/Article"

import { TextField, Button } from '@material-ui/core';
import { History } from 'history';
interface ParamTypes {
    id: string
}
interface Props {
    article: Article
    history: History<unknown>
}
function ArticleForm(props: Props) {
    let { id } = useParams<ParamTypes>()
    const [article, setArticle] = useState<Article>(props.article)

    const [error, setError] = useState<string>('')
    const [loading, setLoading] = useState<boolean>(true)

    function handleClick() {
        articlesService.update(id, props.article)
            .then(() => {
                props.history.push(`/articles/${id}`)
            })
            .catch((err: string) => {
                setError(err)
            })
            .finally(() => setLoading(false))
    }

    return (
        <div className="ma-top-50">
            <Container maxWidth="md">

                <Card>
                    <TextField
                        label="Titre"
                        variant="outlined"
                        required
                        style={{ margin: 8 }}
                        value={article.title}
                        onChange={(e) => setArticle((prev) => {
                            let item = prev
                            item.title = e.target.value
                            return item
                        })}
                    />
                    <TextField
                        label="Text"
                        variant="outlined"
                        required
                        style={{ margin: 8 }}
                        value={article.text}
                        multiline
                        rows={4}
                        onChange={(e) => setArticle((prev) => {
                            let item = prev
                            item.text = e.target.value
                            return item
                        })}
                    />
                    <Button
                        color="primary"
                        variant="contained"
                        onClick={handleClick}
                        disabled={loading}
                    >Update</Button>
                </Card>
            </Container>

        </div>
    );
}

export default ArticleForm;
