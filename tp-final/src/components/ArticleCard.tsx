import Article from "../entities/Article";
import { Button, Card, CardActions, CardContent, GridListTile, Typography } from '@material-ui/core'
interface ArticleCardProps {
    article: Article,
    handleClick: (article: Article) => void
}
function ArticleCard(props: ArticleCardProps) {
    return (
        <div>
            <Card>
                <CardContent>
                    <Typography variant="h5" component="h2">
                        {props.article.title}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => props.handleClick(props.article)}
                    >See More</Button>
                </CardActions>
            </Card>
        </div>
    );
}

export default ArticleCard;