
import { TextField, Button, Grid, Typography, Link } from '@material-ui/core'
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { userService } from '../services';

interface UserProps {
    login?: boolean,
    create?: boolean,
    update?: boolean,
}

// TODO : EMAIL VALIDATION
function UserForm(props: UserProps) {

    const history = useHistory()
    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [name, setName] = useState<string>('')
    const [errorValidation, setErrorValidation] = useState<boolean | undefined>(false)
    const [error, setError] = useState<string>('')
    const [loading, setLoading] = useState<boolean | undefined>(false)
    const [action, setAction] = useState<string>('')
    const [success, setSuccess] = useState<string>('')

    function clearForm() {
        setError('')
        setErrorValidation(false)
        setName('')
        setEmail('')
        setPassword('')
        setSuccess('')
    }
    async function handleClick() {
        setLoading(true)
        try {
            if (props.create) {
                if (!!email && !!password && !!name) {
                    await userService.create({ email, password, name })
                    history.push('/')
                    clearForm()
                } else {
                    setErrorValidation(true)
                }
            } else {
                if (!!email && !!password) {
                    if (props.login) {
                        await userService.login(email, password)
                        history.push('/')
                        clearForm()
                    }
                    if (props.update) {
                        await userService.update({ name, email, password })
                        setSuccess('User updated')
                    }
                } else {
                    setErrorValidation(true)
                }
            }
        } catch (err) {
            setError(err)
            setErrorValidation(false)
        }
        setLoading(false)

    }

    useEffect(() => {
        clearForm()
        if (props.update) {
            setEmail(userService._user.email)
            setPassword(userService._user.password)
            setName(userService._user.name)
            setAction('Update')
        } else if (props.create) {
            setAction('Create')
        } else {
            setAction('Login')
        }
    }, [props])

    return (
        <div>
            <Grid container justify="center">
                <Grid item md={7} className="text-center">
                    <Grid container spacing={1}>
                        {props.update || props.login ? ''
                            :
                            <Grid item xs={6}>
                                <TextField
                                    fullWidth
                                    required
                                    value={name}
                                    label="Name"
                                    variant="outlined"
                                    onChange={(e) => setName(e.target.value)}
                                    style={{ margin: 8 }}
                                    error={errorValidation}
                                    disabled={loading}
                                />
                            </Grid>
                        }

                        <Grid item xs={6}>
                            <TextField
                                fullWidth
                                required
                                value={email}
                                label="Email"
                                variant="outlined"
                                onChange={(e) => setEmail(e.target.value)}
                                style={{ margin: 8 }}
                                error={errorValidation}
                                disabled={loading}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField
                                required
                                fullWidth
                                value={password}
                                label="Password"
                                variant="outlined"
                                onChange={(e) => setPassword(e.target.value)}
                                type="password"
                                style={{ margin: 8 }}
                                error={errorValidation}
                                disabled={loading}
                            />
                        </Grid>
                        {errorValidation ?
                            <Grid item xs={12}>
                                <Typography color="error" className="text-center">Fill the fields</Typography>
                            </Grid>
                            : ''
                        }
                        {error ?
                            <Grid item xs={12}>
                                <Typography color="error" className="text-center">{error}</Typography>
                            </Grid>
                            : ''
                        }
                    </Grid>
                </Grid>
                {props.login ?
                    <Grid item xs={12} md={7} className="text-center pa-bottom-10">
                        <Link href="/sign-in" color="secondary" className="link">No account ? Sign in</Link>
                    </Grid>
                    : ''
                }
                <Grid item xs={12} md={7} className="text-center">
                    <Button
                        color="primary"
                        variant="contained"
                        onClick={handleClick}
                        disabled={loading}
                    >{action}</Button>
                </Grid>
                {success ?
                    <Grid item xs={12} md={7} className="text-center pa-20">
                        <Typography color="primary" className="link">{success}</Typography>
                    </Grid>
                    : ''
                }
            </Grid>

        </div>
    );
}

export default UserForm;
