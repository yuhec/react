import '../App.css'
import { Container } from '@material-ui/core'
import UserForm from '../components/UserForm';

function Auth() {
  
    return (
        <div>
            <Container className="ma-top-50">
                <UserForm login={true} />
            </Container>
        </div>
    );
}

export default Auth;
