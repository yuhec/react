import { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router-dom";
import '../App.css'

import { Container, CircularProgress, GridList, GridListTile, Typography, Box, Grid } from '@material-ui/core'
import Pagination from '@material-ui/lab/Pagination';

import Article from "../entities/Article";
import { articlesService } from "../services";
import ArticleCard from "../components/ArticleCard"

export interface StaticContext {
    statusCode?: number;
}

function Articles(props: RouteComponentProps<any, StaticContext, unknown>) {
    const [loading, setLoading] = useState<boolean>(true)
    const [articles, setArticles] = useState<Article[]>([])

    const [page, setPage] = useState<number>(1)
    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPage(value);
    };

    function loadArticles() {
        setLoading(true)
        articlesService.get(page)
            .then((res: Article[] | []) => {
                setArticles(res)
            })
            .finally(() => setLoading(false))
        // TODO CATCH
    }

    function handleClick(article: Article) {
        props.history.push(`/articles/${article.id}`)
    }

    useEffect(() => {
        loadArticles()
    }, [page])

    return (
        <div>
            <Container maxWidth="lg">
                <div className="ma-top-50 text-center">
                    <Typography variant="h2">
                        Articles
                    </Typography>
                </div>
                {loading ?
                    <div className="text-center ma-top-50">
                        <CircularProgress size={150} />
                    </div>
                    :
                    <div>
                        <GridList cols={3} spacing={5}>
                            {
                                articles.map((article, i) => {
                                    return (
                                        <GridListTile key={article.id}>
                                            <ArticleCard article={article} handleClick={handleClick} />
                                        </GridListTile>
                                    )
                                })
                            }
                        </GridList>
                        <Grid item md={5} className="center pa-20">
                            <Box
                                display="flex"
                                alignItems="center"
                                justifyContent="center"
                            >
                                <Pagination count={articlesService._articles.length/5} color="primary" page={page} onChange={handleChange} />
                            </Box>
                        </Grid>
                    </div>
                }
            </Container>
        </div>
    );
}

export default Articles;
