import {Container, Typography} from '@material-ui/core'
function Home() {
    return (
      <div>
        <Container className="ma-top-50">
          <Typography variant="h1">Home Page</Typography>
        </Container>
      </div>
    );
  }
  
  export default Home;
  