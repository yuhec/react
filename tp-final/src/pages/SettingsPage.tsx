import { Container, Typography, Card } from '@material-ui/core'
import UserForm from '../components/UserForm'
function SettingsPage() {
    return (
        <div>
            <Container className="ma-top-50 text-center">
                <Card className="pa-20">
                    <Typography variant="h3">Settings</Typography>
                    <UserForm update={true} />
                </Card>
            </Container>
        </div>
    );
}

export default SettingsPage;
