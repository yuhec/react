import { Container, Card, CardContent, Typography, CircularProgress, IconButton } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import '../App.css'
import { articlesService, userService } from "../services";
import Article from "../entities/Article"
import CreateIcon from '@material-ui/icons/Create';
import ArticleForm from "../components/ArticleForm";

import { RouteComponentProps } from "react-router-dom";

export interface StaticContext {
    statusCode?: number;
}
interface ParamTypes {
    id: string
}

function Update(article: Article, props: RouteComponentProps<any, StaticContext, unknown>) {
    return(
        <ArticleForm article={article} history={props.history} />
    )
}
function Loading() {
    return (
        <div className="text-center ma-top-50">
        <Card>
            <CircularProgress size={150} />
        </Card>
    </div>
    )
}
function ArticleDetail(props: RouteComponentProps<any, StaticContext, unknown>) {
    let { id } = useParams<ParamTypes>()
    const [article, setArticle] = useState<Article | null>(null)
    const [error, setError] = useState<string>('')
    const [loading, setLoading] = useState<boolean>(true)

    const [update, setUpdate] = useState<boolean>(false)

    function loadArticle() {
        articlesService.getById(id)
            .then((res: Article) => {
                setArticle(res)
            })
            .catch((err) => {
                setError(err)
            })
            .finally(() => setLoading(false))
    }
    useEffect(() => {
        loadArticle()
    }, [id])

    // TODO : if update then display right component

    return (
        <div className="ma-top-50">
            <Container maxWidth="md">
                {loading ?
                    <div className="text-center ma-top-50">
                        <Card>
                            <CircularProgress size={150} />
                        </Card>
                    </div>
                    :
                    <Card>
                        <CardContent>
                            <Typography variant="h3">
                                {!!article ? article.title : ''}
                            </Typography>
                            <IconButton 
                                color="primary" 
                                aria-label="add to shopping cart"
                                disabled={!userService._user.admin}
                                onClick={() => setUpdate(true)}
                            >
                                <CreateIcon />
                            </IconButton>
                        </CardContent>
                        <CardContent>
                            <Typography component="p">
                                {!!article ? article.text : ''}
                            </Typography>
                        </CardContent>
                    </Card>
                }

            </Container>

        </div>
    );
}

export default ArticleDetail;
