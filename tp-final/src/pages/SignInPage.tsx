import { Container } from '@material-ui/core'
import UserForm from '../components/UserForm'

import '../App.css'
function SignIn() {
  
    return (
        <div>
            <Container className="ma-top-50">
                <UserForm create={true}/>
            </Container>
        </div>
    );
}

export default SignIn;
