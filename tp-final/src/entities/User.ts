type User = {
    name: string,
    email: string,
    password: string,
    regular?: Boolean,
    admin?: Boolean,
    super_admin?: Boolean,
}
export default User
  