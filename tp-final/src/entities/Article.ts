type Article = {
    id: string,
    title: string,
    text: string,
}
export default Article
  