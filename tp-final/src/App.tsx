import './App.css';
import React, { useState, useCallback } from 'react'
import {Switch, Route} from 'react-router-dom'
import NotFound from './pages/AuthPage'
import {AuthRouter, MainRouter} from './routes'
import UserContext from './contexts/UserContext';
import { userService } from './services';
import User from './entities/User';

function App() {

  const [userInfos, setUserInfos] = useState<User>({name: '', password: '', email: ''})
  const refreshUserInfos = useCallback( () => {} ,[])

  return (
      <div className="app">
        <UserContext.Provider value={{
          userInfos,
          refreshUserInfos
        }}>
          <Switch>
            <Route path="/404" component={NotFound}/>
            <Route path="/auth" component={AuthRouter}/>
            <Route path="/sign-in" component={AuthRouter}/>
            <Route path="/" component={MainRouter}/>
          </Switch>
        </UserContext.Provider>
      </div>
  );
}

export default App;
