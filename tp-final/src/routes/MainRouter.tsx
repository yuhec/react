import React from 'react';
import {Switch, Route, useHistory, Redirect} from 'react-router-dom'
import {AppBar, Tabs, Tab} from '@material-ui/core';

import { userService } from '../services';
import Header from '../components/Header';

import Home from '../pages/HomePage'
import Articles from '../pages/ArticlesPage'
import ArticleDetail from '../pages/ArticleDetailPage'
import SettingsPage from '../pages/SettingsPage'

const MainRouter = () => {
  const history = useHistory()

  const tabNameToIndex = ["", "articles", "settings"];

  const [selectedTab, setSelectedTab] = React.useState(0);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    history.push(`/${tabNameToIndex[newValue]}`);
    setSelectedTab(newValue);
  };

  if(!userService.isLogged){
    return (
      <Redirect to={"/auth"} />
    )
  }
  return (
    <div>
      <div>
          <Header />
          <AppBar position="static">
            <Tabs  value={selectedTab} onChange={handleChange} aria-label="simple tabs example">
              <Tab label="Home" />
              <Tab label="Articles"  />
              <Tab label="Settings"  />
            </Tabs>
          </AppBar>
        </div>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/articles" render={(props) => <Articles {...props}/>} />
            <Route exact path="/articles/:id" render={(props) => <ArticleDetail {...props}/>} />
            <Route exact path="/settings" render={(props) => <SettingsPage />} />
          <Route path={"*"} render={() => <Redirect to={"/404"}/>}/>
        </Switch> 
    </div>
  );
};

export default MainRouter;