import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import {userService} from "../services";
import Auth from "../pages/AuthPage";
import SignIn from "../pages/SignInPage";
import Header from '../components/Header'

const AuthRouter = () => {
  if(userService.isLogged){
    return (
      <Redirect to={"/"} />
    )
  }
  return (
    <div>
      <Header />
      <Switch>
        <Route path={"/auth"} component={Auth}/>
        <Route path={"/sign-in"} component={SignIn}/>
        <Route path={"*"} render={() => <Redirect to={"/404"}/>}/>
      </Switch>
    </div>
  );
};

export default AuthRouter;