# Laëtitia CONSTANTIN

### users profile
- name: regular, email: r@o.fr, password: regular,
- name: admin, email: a@o.fr, password: admin,
- name: super_admin, email: sa@o.fr, password: super_admin,

## TP final :
    - Done :
        - Authentification (fake API)
        - 3 user types regular, admin, super_admin
        - Manage login errors
        - Sign up to create user (mail, name, password)
        - Main app: 
            -  Blog
                - Articles pagination
                -  Article by id
        - Settings : edit mail and password
        - Header with user infos
    - TODO :
        - Create, Update and delete article if admin
        - Admin settings (only admin)
            - Add user
            - Edit user role
            - Delete user