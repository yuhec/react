import React, { useState } from 'react';
import Square from './Square'

type Props = {
    onClick: (i: number) => void
    squares: string[] | null[]
    player: string
}

function Board(props: Props) {
    const renderSquare = (i: number) => {
      return <Square value={props.squares[i]} onClick={() => props.onClick(i)} />;
    };
  
    return (
      <div>
        <div className="status">Current player : {props.player}</div>
        <div className="board-row">
          {renderSquare(0)}
          {renderSquare(1)}
          {renderSquare(2)}
        </div>
        <div className="board-row">
          {renderSquare(3)}
          {renderSquare(4)}
          {renderSquare(5)}
        </div>
        <div className="board-row">
          {renderSquare(6)}
          {renderSquare(7)}
          {renderSquare(8)}
        </div>
      </div>
    );
  }
export default Board