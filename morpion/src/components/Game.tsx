import React, { useState } from 'react'
import Board from './Board'


function calculateWinner(squares: string[] | null[]) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
        }
    }
    return null;
}

function Game() {
    const [xIsNext, setXIsNext] = useState<boolean>(true)
    const [squares, setSquares] = useState<string[] | null[]>(Array(9).fill(null))
    const [winner, setWinner] = useState<string | null>(null) // plutot useMemo que useState

    // const winner = useMemo(() => {
    //   calculateWinner(squares)
    // }, [xIsNext])
    
    function handleClick(i: number) {
        if (!squares[i]) {
            const current = squares
            current[i] = xIsNext ? 'X' : 'O'
            const winner = calculateWinner(current);
            
            setSquares(current)
            setXIsNext(!xIsNext)
            setWinner(winner)
        }
    }
    function reset() {
        setWinner(null)
        setXIsNext(!xIsNext)
        setSquares(Array(9).fill(null))
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            player={xIsNext ? 'x' : 'o'} 
            onClick={(i: number) => {handleClick(i)}}
            squares={squares}
          />
        </div>
        <div className="game-info">
          <div>{winner != null ? `Le gagnant est ${winner}` : ''}</div>
          <button onClick={reset}>Rejouer</button>
        </div>
      </div>
    );
  }
export default Game