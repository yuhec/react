import React, { useState } from 'react';


type Props = {
    value: string | null,
    onClick: () => void
}

function Square(props: Props) {
    return <button className="square" onClick={props.onClick}>{props.value}</button>;
}

export default Square