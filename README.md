# React courses
© Laëtitia Constantin - 2021 

## What is it ?
You'll find in this repository different exercises made during React's courses.

## Content
 - Morpion Game
    - Done : 
        - Game working, 
        - Display winner
    - Todo : 
        - Create history of each moves
 - Create hooks
    - Done : 
        - Debounce, 
        - Mouse position, 
        - Scroll
    - Todo : 
        - Timer doesn't work yet
 - Todo List
    - Done : 
        - Add task
        - Mark it as done or not
        - Delete it
    - Todo : -
 - Fast and Furious Keyboard
    - Done : 
        - Start and finish the game
        - Display progress bar 
        - Display remaining seconds
        - Display number of the correct words
        - Display stats : nb words, time, words per minute
    - Todo : 
        - Fix latency when game starts
        - Add more stats
        - Add levels on words' length
 - Axios Test : 
    - Done :
        - Display users from an external API (random user generator)
        - Search per nationality
        - Display pagination
    - Todo :
        - Display loading
        - Manage errors
- Context Test WIP :
    - Done :
    - Todo :
        - Header
        - UserSettings component page with a sate to display 2 pages 
        - Fake APIs call
        - Form component with a name
        - Img component for user's profile (only url)
- Router test :
    - Done :
        - MainPage with header ( links settings & articles)
        - Fake APIs call for articles
        - Fake API call for get article by id 
        - Articles list page with ids and contents
        - Click and then display article details
        - Authentification page
        - Fake api call (username)
        - Create 404 with redirection
    - Todo :
        - add img profile




## 1. Create a new project 

Create project and use typescript :

``` npx create-react-app my-app-name --template typescript ```

Add Material UI design :

``` npm install @material-ui/core ```

Add Router :

``` npm install react-router-dom ```


Start running :

``` npm start ```

## 2. Useful links

Markdown : https://www.markdownguide.org/basic-syntax/

Material UI : https://material-ui.com/

Random User Generator : https://randomuser.me/

## 3. Fake APIs call

``` JS
return new Promise((resolve) => {
  setTimeout(() => {
    resolve({name: "Rémi"})
  }, 1000 + Math.floor(Math.random() * 1000))
})
```