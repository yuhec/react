// import {httpService} from './index'
// import RandomUser from '../entities/RandomUser'

// type Params = {
//     results?: number,
//     page?: number,
//     nat?: string | null
// }
// class UserService {
//     getUsers({results=5, page=1, nat=null}: Params) : Promise<RandomUser> {
//         return httpService.get('', {
//             params: {
//                 results,
//                 page,
//                 nat,
//             }
//         })
//     }
// }

class UserService {
    _user = {
      name: "Rémi"
    }
    getUser(){
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(this._user)
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
    updateUser({name}){
      return new Promise((resolve) => {
        setTimeout(() => {
          this._user = {
            ...this._user,
            name
          }
          resolve(this._user)
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
}

const userService = new UserService()

export default userService