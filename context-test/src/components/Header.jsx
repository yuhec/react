import React from 'react'

const Header = ({userName}) => {
    return (
        <header>
            {userName}
        </header>
    )
}
export default Header