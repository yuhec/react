import React, { useCallback, useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
import UserSettings from './pages/UserSettings';
import UserContext from './contexts/UserContext';
import UserSub from './pages/UserSub';

import { Switch, Route } from 'react-router-dom'


function App() {
  const [user, setUser] = useState({name: ""})
  const [loading, setLoading] = useState(true)

  const refreshUserInfos = useCallback(() => {
    setLoading(true)
    userService.getUser()
    .then(
      (res) => {
        setUser(res)
      }
    ).finally(() => setLoading(false))
  }, [])

  useEffect(() => {
    refreshUserInfos()
  }, [refreshUserInfos])
  return (
    <div className="App">
      <UserContext.Provider value={{userInfos: user, loading: loading}}>
        <Switch>
          <Route exact path="/" component={UserSettings} />
          <Route path="/sub/:id" component={UserSub}/>
        </Switch>
        <Header />
        <UserSettings />
      </UserContext.Provider>
    </div>
  );
}

export default App;
