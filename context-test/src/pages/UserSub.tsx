import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

const UserSub = () => {
    const { id } = useParams()
    console.log(id)
    
    useEffect(() => {
        refresh(id)
    }, [id])

    return (
        <div>
            user sub page
        </div>
    )
}

export default UserSub