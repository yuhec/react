import React, { useContext, useEffect, useState } from 'react'
import UserContext from '../contexts/UserContext'

const UserSettings = () => {
    const userContext = useContext(UserContext)
    const [name, setName] = useState('')
    const [loading, setLoading] = useState(false)

    function setNameUser(e) {
        e.preventDefault()
        setLoading(true)
        UserService.updateUser({name})
        .then(() => {
            userContext.refreshUserInfos()
        })
        .finally(() => setLoading(false))
    }

    useEffect(() => {
        setName(userContext.userInfos.name)
    }, [userContext])

    return (
        <div>
            <form onSubmit={setNameUser}>
                <input disabled={userContext.loading || loading} placeholder="Name" value={name} onChange={(e) => setName(e.target.value)}/>
                <button disabled={userContext.loading || loading} type="submit">Update</button>
            </form>
        </div>
    )
}

export default UserSettings