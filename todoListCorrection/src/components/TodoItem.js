import React from 'react';

const TodoItem = ({id, done, name, onClick= () => null, onDelete = () => null}) => {
  return (
    <div className="todo__item">
      <label>
        <input type="checkbox" checked={done} onChange={() => onClick(id)}/>
        {name}
        <button className="btn btn-danger" onClick={() => onDelete(id)}>Delete</button>
      </label>
    </div>
  );
};

export default TodoItem;