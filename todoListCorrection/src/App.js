import React, { useState, useMemo, useCallback } from "react";
import './App.css';
import TodoItem from "./components/TodoItem";

function App(props) {
  const [taskToAdd, setTaskToAdd] = useState("")
  const [tasks, setTasks] = useState([createTask("test")])

  function createTask(name){
    return {
      id: (Math.random() * 123).toString(), //0.3889043984093
      name: name,
      done: false
    }
  }

  function addTask(){
    if(!taskToAdd){
      return
    }
    const newTask = createTask(taskToAdd);
    setTasks(prevTasks => {
      return [...prevTasks, newTask]
    })
    setTaskToAdd("")
  }

  function setToDoneTask(id){
    const newTasks = tasks.map(t => {
      if(t.id !== id){
        return t
      }
      return {
        ...t,
        done: true
      }
    })
    setTasks(newTasks)
  }


  function setToUndoneTask(id){
    const newTasks = tasks.map(t => {
      if(t.id !== id){
        return t
      }
      return {
        ...t,
        done: false
      }
    })
    setTasks(newTasks)
  }

  function deleteTaskById(id){
    setTasks(prevTask => {
      return prevTask.filter(t => {
        if(t.id === id){
          return false
        }
        return true
      })
    })
  }



  //map/filter/reduce
  const tasksDone = useMemo(() => {
    return tasks.filter(task => {
      if(task.done){
        return true
      } else {
        return false
      }
    })
  },[tasks])

  const tasksUndone = useMemo(() => {
    return tasks.filter(task => {
      if(task.done){
        return false
      } else {
        return true
      }
    })
  },[tasks])



  return (
    <div className="container">
      <div className="row d-flex justify-content-center">
        <div className="form-group">
          <input value={taskToAdd} onChange={(e) => setTaskToAdd(e.target.value)}/>
          <button className="btn btn-primary" onClick={() => addTask()}>Add task</button>
        </div>
      </div>
      <div className="row">
      <div className="col-5">
        TODO
        {
          tasksUndone.map(t => {
            return (
              <TodoItem key={t.id} id={t.id} done={t.done} name={t.name} onClick={setToDoneTask} onDelete={deleteTaskById}/>
            )
          })
        }
      </div>
      <div className="col-5">
        DONE
        {
          tasksDone.map(t => {
            return (
              <TodoItem key={t.id} id={t.id} done={t.done} name={t.name} onClick={setToUndoneTask} onDelete={deleteTaskById}/>
            )
          })
        }
      </div>
    </div>
    </div>
  );
}


export default App;
