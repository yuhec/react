import React from 'react'
import './Todo.css'
import { useState } from "react"
import { TextField, Card, Button, Grid } from '@material-ui/core';
import Item from './Item'


import List from '@material-ui/core/List';

type Task = {
    id: string,
    text: String,
    checked: boolean,
}
const Todo = () => {

    const [task, setTask] = useState<string>('')
    const [tasks, setTasks] = useState<Task[]>([])

    function createTask(text: string) {
        return {
            id: (Math.random() * 123).toString(),
            text,
            checked: false,
        }
    }

    function addItem() {
        if (!task) {
            return
        }
        const current = tasks
        current.push(createTask(task))
        setTasks(current)
        setTask('')
    }
    function updateCheck(id: string) {
        const newTasks =  tasks.map((t: Task) => {
            if (t.id !== id) {
                return t
            }
            return {
                ...t,
                checked: true
            }
        })
        setTasks(newTasks)
        
    }
    function removeItem(id: string) {
        setTasks(prevTask => {
            return prevTask.filter(t => {
              if(t.id === id){
                return false
              }
              return true
            })
        })
    }
    
    return (
        <div>
            <Card className="todo">
                <TextField label="Tâche à réaliser" variant="outlined" value={task} onChange={(e) => { setTask(prevState => {
                    let task = prevState
                    task = e.target.value
                    return task
                }) }}></TextField>
                <br />
                <Button variant="contained" color="primary" onClick={addItem}>Sauvegarder</Button>
            </Card>
            <Card className="todoList">
                <Grid container justify="center">
                    <Grid item xs={5} >
                        <List dense>
                            <p>Todo List :</p>
                            {
                                tasks.map((task: Task) => {
                                    return (
                                        <div>
                                            <Item 
                                            id={task.id}
                                            checked={task.checked}
                                            text={task.text}
                                            onClick={updateCheck}
                                            onDelete={removeItem}
                                            />
                                        </div>
                                    )
                                })
                            }
                        </List>
                    </Grid>
                </Grid>
            </Card>
        </div>
    )
}
export default Todo;