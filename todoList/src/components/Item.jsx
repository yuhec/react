import React from 'react'
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteIcon from '@material-ui/icons/Delete';

const Item = (props) => {
    return (
        <div>
            <ListItem className={props.checked ? 'itemCheck' : ''}>
                <ListItemIcon>
                    <Checkbox
                        checked={props.checked}
                        onClick={() => props.onClick(props.id)}
                    />
                </ListItemIcon>
                <ListItemText>{props.text}</ListItemText>
                <ListItemIcon>
                    <DeleteIcon
                        onClick={() => props.onDelete(props.id)}
                    />
                </ListItemIcon>
            </ListItem>
        </div>
       
    )
}
export default Item;