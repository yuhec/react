import logo from './logo.svg';
import './App.css';
import {useEffect, useState} from "react"
import Button1 from './components/Button';
import Todo from './components/Todo';
// import Form from './components/TodoList/Form';
// import List from './components/TodoList/List';
import {Button, Container} from '@material-ui/core';


// TODO : créer un input rentrer le nom
// bouton reset & afficher le nom
function App(props) {
  const [userName, setUsername] = useState(props.name)

  // Va s'exécuter une fois au début du chargement
  useEffect(() => {

  }, []) // si deps [userName] alors ça va se réexécuter à chaque changement de userName 
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        
        <div>
          <input placeholder="Rentrer un nom" value={userName} onChange={(e) => { setUsername(e.target.value)}}></input>
          <button onClick={() => setUsername('')}>Reset</button>
          <p>Coucou {userName}</p>
          <Button1>Je suis un bouton</Button1>
          <Button variant="contained" color="primary">Bouton Material UI</Button>
        </div>
        <Container>
          <Todo></Todo>
        </Container>
      </header>
     
      {/* <Form></Form>
      <List></List> */}

    </div>
  );
}

export default App;
