import { useEffect, useState } from "react"

function useMousePosition() {
    const [mousePosition, setPosition] = useState({x: 0, y: 0})
    useEffect(() => {
        const handler = () => {
            setPosition({x: window.event.x, y:window.event.y})
        }
        window.addEventListener('mousemove', handler)
        return () => {
            window.removeEventListener('mousemove', handler)
        }
    }, []) 
    return mousePosition
}
export default useMousePosition