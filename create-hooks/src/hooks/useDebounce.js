import { useEffect, useState } from "react";

function useDebounce(value, delay) {


  const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        const timeOut = setTimeout(() => {
            setDebouncedValue(value)
        }, delay);
        return () => {
            clearTimeout(timeOut)
        }
    }, [value, delay])
    
    return debouncedValue
}
export default useDebounce