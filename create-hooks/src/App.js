import logo from './logo.svg';
import './App.css';
import useScroll from './hooks/useScroll';
import useDebounce from './hooks/useDebounce';
import { useState } from 'react';
import useMousePosition from './hooks/useMousePosition';
import useTimer from './hooks/useTimer';

function App() {
  const nb = 20
  const list = Array(nb).fill('Ceci est un carré')

  const scroll = useScroll()

  const [search, setSearch] = useState('')
  const searchDebounced = useDebounce(search, 1000)

  const mousePosition = useMousePosition()

  const timer = useTimer(10000, 50)


  return (
    <div className="App">
      <header className="App-header">
        <div className="scroll">{scroll}</div>
        <div className="mouse">{mousePosition.x} - {mousePosition.y}</div>
        <input 
          value={search} 
          onChange={(e) => {
            setSearch(e.target.value)
          }}
        />
        <p>{searchDebounced}</p>
        {timer}
        <div>
          {
            list.map((e,i) => {
              return (
                <div className="square" key={i}>
                  <img src={logo} className="App-logo" alt="logo" />
                </div>
              )
            })
          }
        </div>
      </header>
    </div>
  );
}

export default App;
