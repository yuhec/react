import './App.css';
import React from 'react'
import {Switch, Route} from 'react-router-dom'
import NotFound from './pages/Auth'
import {AuthRouter, MainRouter} from './routes'

function App() {

  return (
      <div className="app">
        <Switch>
          <Route path="/404" component={NotFound}/>
          <Route path="/auth" component={AuthRouter}/>
          <Route path="/" component={MainRouter}/>
        </Switch>
      </div>
  );
}

export default App;
