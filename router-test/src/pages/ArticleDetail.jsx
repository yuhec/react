import { Container, Card, CardContent, Typography } from "@material-ui/core";
import '../App.css'
function ArticleDetail(props) {
    console.log(props)
    return (
        <div className="ma-top-50">
            <Container maxWidth="md">
                <Card>
                    <CardContent>
                        <Typography variant="h3">
                            {props.location.state.item.title}
                        </Typography>
                    </CardContent>
                    <CardContent>
                        <Typography component="p">
                            {props.location.state.item.text}
                        </Typography>
                    </CardContent>
                </Card>
            </Container>

        </div>
    );
}

export default ArticleDetail;
