import { Container, TextField, Button, Grid, Typography } from '@material-ui/core'
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { userService } from '../services';
import '../App.css'
function Auth() {
    console.log('auth')
    const history = useHistory()
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)

    function login() {
        if (!!name && !!password) {
            setLoading(true)
            userService.login(name, password)
            .then(() => {
                history.push('/')
            })
            .finally(() => setLoading(false))
        } else {
            setError(true)
        }
       
    }
    return (
        <div>
            <Container>
                <Grid container justify="center">
                    <Grid item md={7} className="text-center">
                        <Grid container spacing={1}>
                            <Grid item xs={6}>
                                <TextField 
                                    fullWidth
                                    required 
                                    value={name} 
                                    label="Name" 
                                    variant="outlined" 
                                    onChange={(e) => setName(e.target.value)} 
                                    style={{ margin: 8 }}
                                    error={error}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField 
                                    required
                                    fullWidth
                                    value={password} 
                                    label="Password" 
                                    variant="outlined" 
                                    onChange={(e) => setPassword(e.target.value)} 
                                    type="password" 
                                    style={{ margin: 8 }}
                                    error={error}
                                />
                            </Grid>
                            { error ? 
                                <Grid item xs={12}>
                                    <Typography color="error">Veuillez remplir les champs</Typography>
                                </Grid> 
                                : ''
                            }
                        </Grid>
                    </Grid>
                    <Grid item md={7} className="text-center">
                        <Button color="primary" variant="contained" onClick={login}>Login</Button>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}

export default Auth;
