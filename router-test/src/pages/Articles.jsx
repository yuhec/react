import { useEffect, useState } from "react";
import { articlesService } from "../services";

import { Container, Button, Card, CardActions, CardContent, CircularProgress, GridList, GridListTile, Typography } from '@material-ui/core'

function Articles(props) {
    console.log(props)
    const [loading, setLoading] = useState(true)
    const [articles, setArticles] = useState([])

    function loadArticles() {
        articlesService.get()
            .then((res) => {
                setLoading(true)
                console.log(res)
                setArticles(res)
            })
            .finally(() => setLoading(false))
    }

    function handleClick(item) {
        props.history.push(`/articles/${item.id}`, { item })
    }

    useEffect(() => {
        loadArticles()
    }, [])
    return (
        <div>
            <Container maxWidth="lg">
                <div className="ma-top-50 text-center">
                    <Typography variant="h2">
                        Articles
                    </Typography>
                </div>
                {loading ?
                    <div className="text-center ma-top-50">
                        <CircularProgress size={150} />
                    </div>
                    :
                    <GridList cols={3}>
                        {
                            articles.map((article, i) => {
                                return (
                                    <GridListTile key={i}>
                                        <Card>
                                            <CardContent>
                                                <Typography variant="h5" component="h2">
                                                    {article.title}
                                                </Typography>
                                            </CardContent>
                                            <CardActions>
                                                <Button variant="contained" color="secondary" onClick={() => handleClick(article)}>See More</Button>
                                            </CardActions>
                                        </Card>
                                    </GridListTile>
                                )
                            })
                        }
                    </GridList>
                }
            </Container>
        </div>
    );
}

export default Articles;
