import {Container, Typography} from '@material-ui/core'
function Home() {
    return (
      <div>
        <Container>
          <Typography variant="h1">Home Page</Typography>
        </Container>
      </div>
    );
  }
  
  export default Home;
  