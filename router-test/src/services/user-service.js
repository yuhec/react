class UserService {
    _user = {
      name: "Rémi"
    }
    isLogged = false
    login(name, password) {
      return new Promise((resolve) => {
        this.isLogged = true
        setTimeout(() => {
          this._user.name = name
          resolve(this._user)
        }, 1000)
      })
    }
    logout() {
      return new Promise((resolve) => {
        this.isLogged = false
        setTimeout(() => {
          this.isLogged = false
          resolve()
        }, 1000)
      })
    }
    getUser(){
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          if (this.isLogged) {
            resolve(this._user)
          } else {
            reject({err: "You are not logged"})
          }
        }, 1000)
      })
    }
}
const userService = new UserService()
export default userService