import {v4 as uuidv4} from 'uuid'
import { LoremIpsum } from 'lorem-ipsum'

const lorem = new LoremIpsum({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 12,
    min: 7
  }
});

function generateArticle() {
  return {
    id: uuidv4(),
    title: lorem.generateSentences(1),
    text: lorem.generateParagraphs(3)
  }
}

class ArticlesService {
    _articles = [
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
      generateArticle(),
    ]
    get(){
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(this._articles)
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
    getById({id}){
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(this._articles.find(article => article.id === id))
        }, 1000 + Math.floor(Math.random() * 1000))
      })
    }
}
const articlesService = new ArticlesService()
export default articlesService